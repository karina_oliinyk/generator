function worker(gen, ...args) {
  const iter = gen(...args);
  
  function recFn(val) {
    if (!(val instanceof Object)) {
      res = iter.next(val);
    }

    if (res.done) {
      return res.value;
    }

    if (val instanceof Object) {
      return val.value.then(recFn);
    } 
    
    if (res.value instanceof Promise ) {
      try {
      return res.value.then(recFn);
      } catch {
        return recFn(iter.throw());
      }
    } 
    
    if (typeof res.value === 'function') {
      try {
        return recFn(res.value());
      } catch {
        return recFn(iter.throw());
      }
    }

    return recFn(res.value);
  }

  return Promise.resolve(recFn());
}